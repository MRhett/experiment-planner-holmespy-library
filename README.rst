=============
Release Notes
=============

-------------------------
Release Notes: 2018.12.15
-------------------------

* Added the ability to specify how to select action in MCTS policy depending
  on child values using the ``final_policy_type`` argument. Valid values
  are ``valid_policy_type = "exploit"`` (Default) or ``valid_policy_type =
  "ucb"``. The previous implementation only did the UCB policy for picking
  the final action.


-------------------------
Release Notes: 2018.12.15
-------------------------

* Added ``calculate_oc`` in ``simulators.py`` to calculate OC for a list of
  simulations.

* Simulations are now saved in bzipped format by default. Pass ``zip_file =
  False`` in ``save`` and ``unzip_file = False`` in ``load`` to
  disable. 

* :class:`MCTSPolicy` now requires a specification of noise model in its 
  constructor in order to do simulations.




-------------------------
Release Notes: 2018.12.09
-------------------------

* Updated GaussianProcessBeliefModel to implement new BeliefModel Interface

-------------------------
Release Notes: 2018.11.23
-------------------------

*Code Breaking Changes*

* Belief models must now implement ``mean(x)``, which returns the mean
  estimate of the response at a particular input ``x``. It must also implement
  ``mean_f`` which returns a callable function representing the mean estimate.
  That is, the functionality of previous releases ``mean`` functions will now
  be called ``mean_f``.


* ``GaussianProcessBeliefModel`` is temporarily incomplete until we can test the
  current implementation. Calls to ``mean, covariance and covariance_f`` will
  raise an ``NotImplementedError``.

-------------------------
Release Notes: 2018.11.20
-------------------------

*Code Breaking Changes*

* Policies will now require a ``PriorState`` as input argument instead of
  a ``BeliefModel`` to call ``suggest``.

* ``PriorState.available_actions`` must return an ``AlternativeSet``

* ``PriorState`` has a new function ``current_beliefs`` that returns a
  ``BeliefModel``. Any class that derives from ``PriorState`` must implement
  this function. From now on, we'll assume that all states have a belief state.

* The ``e_thr`` argument to initialize a ``_MCTSPostDecisionNode`` is 
  no longer optional

* ``MCTSPolicy`` now takes in parameter ``e_thr`` in its constructor.

* To propogate ``e_thr`` in  ``_MCTSPriorNode.update``, ``_MCTSPriorNode``
  now takes ``e_thr`` in its constructor

* The ``comp_budget`` in MCTSPolicy is no longer optional.

* Rewritten ``LookupTableBeliefModel`` class.

  * You now must supply an ``AbstractDiscreteAlternativeSet`` object as an 
    argument to the constructor of ``LookupTableBeliefModel``. Built-in
    implementing subclasses of ``AbstractDiscreteAlternativeSet`` are:

    * ``DiscreteAlternativeSet``
    * ``DiscreteGridAlternativeSet``

  * Functions that are returned from ``LookupTableBeliefModel.mean()``,
    ``LookupTableBeliefModel.sample()``, 
    ``LookupTableBeliefModel.covariance()`` now expect input values from the
    passed in ``AbstractDiscreteAlternativeSet``, instead of an index.

* ``BeliefStateParameters``, ``BeliefPriorState`` and 
  ``BeliefPostDecisionState`` moved into library. ``BeliefStateParameters``
  now takes in 

  * ``N`` the number of experiments to run
  * ``A`` an ``AlternativeSet`` describing the available actions
  * ``sigma2W`` a Python function that returns the measurement variance 
    as a function of action.
  * ``final_reward`` a Python function that takes in a ``BeliefModel`` and
    returns the reward of being in that belief state at the end.

* ``SimulationParams`` renamed ``SimulationParameters``


The following changes won't break code:

* MCTSPolicy now will accept an optional ``simulation_policy`` argument as
  input to its constructor. This policy is used in simulations. It will default
  to pure exploration.

* Implemented Policies:

  * ``ExploitationPolicy``
  * ``MaxVariancePolicy``
  * ``UCBPolicy``

* Rewritten ``DiscreteAlternativeSet`` class:

  * added support to call ``X[i]``, which returns the ``i``-th element of the
    discrete list

  * added ``X.indexof(x)``, which returns the index of element ``x``.


* ``Simulator`` class now has a ``verbose`` optional argument. If set to 
  ``True``, then the simulator will output some information during the 
  simulation run.


