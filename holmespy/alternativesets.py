from abc import ABC, abstractmethod
import random
import numpy as np

class AlternativeSet(ABC):

    @abstractmethod
    def sample(self):
        pass

class AbstractDiscreteAlternativeSet(AlternativeSet):

    @abstractmethod
    def size(self):
        pass

    @abstractmethod
    def indexof(self, x):
        pass

    @abstractmethod
    def remove(self, x):
        pass

    @abstractmethod
    def __contains__(self, x):
        pass

    @abstractmethod
    def __iter__(self, x):
        pass

    @abstractmethod
    def __getitem__(self, key):
        pass


class DiscreteAlternativeSet(AbstractDiscreteAlternativeSet):

    def __init__(self, X):
        self.X = X

        # build dictionary to do reverse search
        self.indices = {}
        for (i,x) in enumerate(X):
            self.indices[x] = i

    def sample(self):
        i = np.random.randint(0, self.size())
        return self[i]

    def size(self):
        return len(self.X)

    def remove(self, x_to_remove):
        ret_val = []
        for x in self.X:
            if(x  != x_to_remove):
                ret_val.append(x)
        return DiscreteAlternativeSet(ret_val)

    def __len__(self):
        return self.size()

    def indexof(self, x):
        return self.indices[x]

    def __contains__(self, a):
        return self.X.__contains__(a)

    def __iter__(self):
        # Supports iteration over set
        return self.X.__iter__()

    def __getitem__(self, key):
        # Supports indexing into set
        return self.X[key]


class ContinuousAlternativeSet(AlternativeSet):
    def __init__(self, bounds):
        self._bounds = bounds

    def sample(self):
        D = len(self._bounds)
        x = np.zeros([D])

        for i in range(D):
           x[i] = np.random.uniform(self._bounds[i][0], self._bounds[i][1])

        return x

    def bounds(self):
        return self._bounds



class DiscreteGridAlternativeSet(AbstractDiscreteAlternativeSet):
    """
    Represents a multi-dimensional grid, without allocating
    each point in memory. 
    """

    def __init__(self, bounds, num_points):
        """
        Constructor

        Parameters
        ----------

        bounds : A list of pairs.
            Each pair representing the minimum and maximum (inclusive) values
            of a single dimension in a grid. The total number of dimensions
            equals the length of this list

        num_steps : list
            The number of points for each dimension. The total number of points
            on the grid is the product of all entries in this list.
        """

        assert(all([isinstance(s, int) for s in num_points]))
        assert(len(bounds) == len(num_points))

        self.bounds = bounds
        self.num_points = num_points
        self.dim = len(bounds)
        self._size = np.prod(self.num_points)
        self.stepsizes = [ (b[0][1] - b[0][0])/(b[1]-1)\
                for b in zip(bounds, num_points) ]

    def sample(self):
        """
        Returns a grid point, sampled uniformly at random from the finite set
        of grid points this set represents.
        """
        x = [ b[0][0] + np.random.randint(0, b[1])*b[2] \
                for b in zip(self.bounds, self.num_points, self.stepsizes)]
        return np.array(x)

    def size(self):
        """
        The total number of grid points
        """
        return self._size

    def __len__(self):
        return self.size()

    def __iter__(self):
        self.cursor = 0
        return self

    def __next__(self):
        if(self.cursor >= self._size):
            raise StopIteration
        m_idx = self.cursor
        x = self.__getitem__(m_idx)
        self.cursor += 1
        return x

    def __getitem__(self, m_idx):
        if(m_idx >= self._size) or (m_idx < 0):
            raise IndexError

        # Convert multi-index to a point
        x = np.zeros([self.dim])
        for i in range(self.dim):
            idx = m_idx % self.num_points[i]
            x[i] = self.bounds[i][0] + idx*self.stepsizes[i]
            m_idx = m_idx // self.num_points[i]
        return x


