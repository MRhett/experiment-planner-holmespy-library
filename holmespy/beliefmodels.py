"""
This module contains belief model classes.
"""

from abc import ABC, abstractmethod
import numpy as np

from sklearn.gaussian_process import GaussianProcessRegressor
from deprecated import deprecated


class BeliefModel(ABC):
    """
    This represents the abstract belief model interface, and outlines
    the functions any belief model must implement
    """

    @abstractmethod
    def update(self, x, y, sigma2W):
        """
        Calculates the bayesian update of the current beliefs stored
        in this object, given an observation y for an alternative x.
        :param x: the experiment that was run
        :param y: the experimental result
        :param sigma2W: the noise variance
        :return: a new BeliefModel that has the updated beliefs
        """

    @abstractmethod
    def sample_f(self):
        """
        Return a ground truth sampled from current beliefs, using
        representation defined by belief model
        """

    @abstractmethod
    def mean(self, x):
        """
        Returns the mean estimate of the response, for current beliefs
        at a particular alternative ``x``.
        :param x: the alternative to calculate the mean estimate response
        :type x: Type depends on the belief model.
        """

    @abstractmethod
    def covariance(self, x, y):
        """
        Returns the covariance Cov(f(x), f(y)), based on current beliefs.
        """

    @abstractmethod
    def variance(self, x):
        """
        Returns the variance Var(f(x)) based on current beliefs. Defaults to
        calling the class method `self.covariance(x,x)`.
        """

    @abstractmethod
    def std(self, x):
        """
        Returns the standard deviation std( f(x) ) based on current beliefs.
        Defaults to returning np.sqrt(self.variance(x)).
        """

    @deprecated
    def mean_f(self):
        """
        The mean function as a callable python function
        """
        return self.mean

    @deprecated
    def covariance_f(self):
        """
        The covaraince function as a callable python function
        """
        return self.covariance

    @deprecated
    def variance_f(self):
        """
        The varaince function as a callable python function
        """
        return self.variance

    @deprecated
    def std_f(self):
        """
        The std function as a callable python function
        """
        return self.std


class LookupTableBeliefModel(BeliefModel):
    """
    Implements lookup-table beliefs, which represents beliefs on
    some number of random variables representing some response function
    values at some number of finite points.
    """

    def __init__(self, mu_0, Sigma_0, actions):
        """
        Initializes with prior belief
        :param mu_0: prior mean, an numpy 1D array of length D
        :param Sigma_0: prior covariance, a numpy 2D array of size DxD
        :param X: The discrete objects this Belief Model is representing some
        function value over
        :type X: :class:`DiscreteAlternativeClass`
        """

        # perform some simple checks:
        self.num_dim = len(mu_0)

        # save mean and covariance
        self.mu_0 = mu_0
        self.Sigma_0 = Sigma_0
        self.actions = actions
        self.chol = np.linalg.cholesky(Sigma_0)
        self._std = np.sqrt(np.diag(Sigma_0))

    def sample_f(self):
        """
        Samples from a the multivaraite normal distribution defined by prior
        mean and covariance. Converts the vector into a callable python
        function.
        """
        # sample from multivariate normal distribution
        f_vec = self.mu_0 + self.chol @ np.random.normal(0, 1, self.num_dim)
        return LookupTableBeliefModel._VecFunc(f_vec, self.actions)

    def update(self, x, y, sigma2W):
        """
        Performs the bayesian update for multivariate normal distributions,
        returns a new LookupTableBeliefModel corresponding to posterior beliefs
        :param x: the experiment that was run, type depends on input (number
            or vector)
        :param y: the observation that was made, a number
        :param sigma2W: the variance of the measurement noise associated with
            this observation, a number
        """

        idx = self.actions.indexof(x)

        gamma = 1/(sigma2W + self.Sigma_0[idx, idx])
        posterior_mean = self.mu_0 + \
                (y - self.mu_0[idx])*gamma*self.Sigma_0[:, idx]
        posterior_covariance = self.Sigma_0 - \
                np.dot(gamma, np.outer(self.Sigma_0[:, idx],
                                       self.Sigma_0[idx, :]))

        # return a lookuptable belief model with posterior beliefs
        return LookupTableBeliefModel(posterior_mean,
                                      posterior_covariance,
                                      self.actions)

    def mean(self, x):
        return self.mu_0[self.actions.indexof(x)]

    def std(self, x):
        return self._std[self.actions.indexof(x)]

    def covariance(self, x, y):
        return self.Sigma_0[self.actions.indexof(x), self.actions.indexof(y)]

    def variance(self, x):
        idx = self.actions.indexof(x)
        return self.Sigma_0[idx, idx]

    class _VecFunc:
        """
        A callable wrapper around a discretized function with one argument
        """
        #pylint: disable=too-few-public-methods

        def __init__(self, f_vec, actions):
            self.f_vec = f_vec
            self.actions = actions

        def __call__(self, x):
            return self.f_vec[self.actions.indexof(x)]



class LinearBeliefModel(BeliefModel):
    r"""
    The class represents the linear belief model for an response. That is, for
    an input x, the response is modeled as:

    $$ f(x) = \sum_{i=1}^D \theta_i \phi_i(x),$$

    where $\phi_i$ are basis functions and $\theta_i$ are coefficients for
    which we maintain Bayesian beliefs. In this class, we assume the $\theta_i$
    have a multivariate normal distribution.

    :param mu_0: mean vector for the multivariate normal distribution on
    parameters $\theta$
    :type mu_0: a 1D numpy.ndarray

    :param Sigma_0: the covariance matrix for the multivariate normal
    distribution
    :type Sigma_0: a 2D numpy.ndarray

    :param basis_functions: Basis functions to transform input x. The number of
    basis functions should be the same as the dimension of theta.
    :type basis_functions: A list of callable python functions which each
    return a response function value.
    """

    def __init__(self, mu_0, Sigma_0, basis_functions):
        assert len(mu_0) == Sigma_0.shape[0]
        assert len(mu_0) == Sigma_0.shape[1]

        self.mu_0 = mu_0
        self.Sigma_0 = Sigma_0
        self.basis_functions = basis_functions

        # preallocate array
        self.phi_x = np.zeros(len(mu_0))
        self.phi_y = np.zeros(len(mu_0))

    def sample_f(self):
        """
        Return a ground truth sampled from current beliefs, using
        representation defined by belief model
        """
        # sample a theta
        theta = np.random.multivariate_normal(self.mu_0, self.Sigma_0)
        return self._LinearModelMeanFunction(theta, self.basis_functions)


    def update(self, x, y, sigma2W):
        """
        Calculates the Bayesian update of the current beliefs stored in this
        object, given an observation y for an action x.

        :param x: the action that was taken.
        :type x: varies

        :param y: the experimental result
        :type y: float

        :param sigma2W: the measurement noise level (variance)
        :type sigma2W: float, positive.

        :return: a new LinearBeliefModel that has the updated beliefs
        """

        # Bayesian Update Formulas
        alpha = (y - self.mean(x))/(sigma2W + self.covariance(x, x))

        for idx, phi in enumerate(self.basis_functions):
            self.phi_x[idx] = phi(x)
        cov_phi = (self.Sigma_0 @ self.phi_x)

        posterior_mean = self.mu_0 + alpha * cov_phi
        posterior_cov = self.Sigma_0 - alpha*np.outer(cov_phi, cov_phi)

        return LinearBeliefModel(posterior_mean,
                                 posterior_cov,
                                 self.basis_functions)


    def mean(self, x):
        # map-reduce
        for idx, phi in enumerate(self.basis_functions):
            self.phi_x[idx] = phi(x)
        return np.dot(self.mu_0, self.phi_x)


    def covariance(self, x, y):
        # map-reduce
        for idx, phi in enumerate(self.basis_functions):
            self.phi_x[idx] = phi(x)
            self.phi_y[idx] = phi(y)
        return self.phi_y @ (self.Sigma_0 @ self.phi_x)


    def variance(self, x):
        # map-reduce
        for idx, phi in enumerate(self.basis_functions):
            self.phi_x[idx] = phi(x)
        return self.phi_x @ (self.Sigma_0 @ self.phi_x)


    def std(self, x):
        # Inline covariance calculation to avoid function call overhead
        # map-reduce
        for idx, phi in enumerate(self.basis_functions):
            self.phi_x[idx] = phi(x)
        return np.sqrt(self.phi_x @ (self.Sigma_0 @ self.phi_x))


    class _LinearModelMeanFunction():
        """
        A callable class reprenting a sampled function from this belief.
        """
        #pylint: disable=too-few-public-methods

        def __init__(self, theta, basis_functions):
            self.theta = theta
            self.basis_functions = basis_functions
            self.phi_x = np.zeros(len(theta))

        def __call__(self, x):
            # populate arrays
            for idx, phi in enumerate(self.basis_functions):
                self.phi_x[idx] = phi(x)
            return np.dot(self.phi_x, self.theta)


from sklearn.gaussian_process.kernels import RBF

class GaussianProcessBeliefModel(BeliefModel):
    """
    Non-parametric gaussian estimation, like the lookup-table model but
    supported on points at which we have data, instead of a fixed set of points
    (as is the case of the lookup-table model). Thus the model size grows with
    respect to number of data points.


    Args:
        mu_0 (float): The prior mean. This belief model assumes a constant 
            prior mean.
        var_0 (float): The prior variance, or uncertainty on the constant 
            prior mean. The diagonal elements of the prior covariance function 
            take on this value.
        length_scales (np.array): The length scales used to normalize
            input variables when computing the distance between two inputs. 
            One value for each input variable.
        support (np.array): The inputs points used to sample a function from
            this distribution. This should be a np.array of size n x d, where
            n is just the number of support points and d is the number of input
            variables.
        data (np.array): The data matrix.  Each row corresponds a data point.
            The first few columns describe the input variables, the
            second-to-the-last column describes the observed response, and the
            last column is the noise variance assigned to the data point. If
            the inputs are d dimensional, and there are n data points, then
            data is an (d+2) x n dimensional matrix. Optional, default = None.
        num_hyper_parameter_fits (int): The number of times to fit the
            hyperparameters of the kernel function when fitting to data. 
            Should be a non-negative integer. Optional, default = 0. If
            not specified, then non hyperparameter optimization occurs.

    """

    def __init__(self, mu_0, var_0, length_scales, support, data=None,
                 num_hyperparameter_fits=0):

        #pylint: disable=too-many-arguments

        # Prior
        self.mu_0 = mu_0
        self.var_0 = var_0
        self.length_scales = length_scales
        kernel = var_0*RBF(length_scales)

        # Instantiate GuassianProcessRegressor for sklearn.
        args = {
            "kernel": kernel,
            "copy_X_train":  False,
        }

        if num_hyperparameter_fits == 0:
            args['optimizer'] = None
        else:
            args['n_restarts_optimizer'] = num_hyperparameter_fits - 1

        has_data = (data is not None) and (data.shape[0] > 0)
        if has_data:
            args['alpha'] = data[:, -1]

        self.gpr = GaussianProcessRegressor(**args)

        # Fit to data if provided
        if has_data:
            self.gpr.fit(data[:, :-2], data[:, -2] - self.mu_0)

        self.data = data
        self.support = support
        self.num_hyperparameter_fits = num_hyperparameter_fits


    def update(self, x, y, sigma2W):
        """ Bayesian update for Gaussian Processes

        Calculates the bayesian update of the current beliefs stored in this
        object, given an observation y for an alternative x.

        :param x: The input data
        :type x: a 1D numpy.ndarray

        :param y: The observation (scalar)
        :type y: ``float``

        :param sigma2W: The measurement variance associated to the observation
        :type sigma2W: ``float``

        """

        # new data point
        row = np.zeros([1, len(x) + 2])
        row[0, :-2] = x
        row[0, -2] = y
        row[0, -1] = sigma2W

        # append new data point
        if self.data is None:
            self.data = np.zeros([0, len(x) + 2])
        new_data = np.append(self.data, row, axis=0)

        return GaussianProcessBeliefModel(\
                self.mu_0, self.var_0, self.length_scales, self.support,
                data = new_data, 
                num_hyperparameter_fits=self.num_hyperparameter_fits)

    def sample_f(self):
        """ Samples a function from the Gaussian process.

        The function will return function that interpolates from these sampled
        points, which uses another Gaussian process to do this interpolation.
        """

        # fit another GP to the sampled points. We'll asssume no noise
        # from the sampling.
        sampled_gpr = GaussianProcessRegressor(\
                kernel=(self.gpr.kernel_ if hasattr(self.gpr, "kernel_") \
                                         else self.gpr.kernel),
                alpha=1e-10,
                copy_X_train=False,
                optimizer=None)

        # Support is a combo of self.support and historical data
        support = np.vstack((self.support, self.data[:,:-2]))

        # Sample the outputs, then fit
        sampled_values = self.gpr.sample_y(support, random_state=None)
        sampled_gpr.fit(support, sampled_values.flatten())

        # Return a callable function based on this new GP
        return self._MeanGPFunction(sampled_gpr, self.mu_0)


    def mean(self, x):
        mean = self.gpr.predict([x])[0] + self.mu_0
        return mean


    def covariance(self, x, y):
        _, cov = self.gpr.predict([x, y], return_cov=True)
        return cov[0, 0]


    def variance(self, x):
        _, cov = self.gpr.predict([x, x], return_cov=True)
        return cov[0, 0]


    def std(self, x):
        _, std = self.gpr.predict([x], return_std=True)
        return std[0]

    class _MeanGPFunction:
        """
        A callable class used by the mean function. Used instead of a lambda
        function to allow for serialization.
        """

        #pylint: disable=too-few-public-methods

        def __init__(self, gpr, mu_0):
            self.gpr = gpr
            self.mu_0 = mu_0

        def __call__(self, x):
            mean = self.gpr.predict([x])[0] + self.mu_0
            return mean


@deprecated
class MultiBeliefModel(BeliefModel):
    r"""
    Multidimensional, independent beliefs

    This class represents beliefs on functions f^\star : X -> R^m, where each
    dimension of the outcome is considered independent from other dimensions.
    That is, if x, x' are in X, and f_i, f_j are component scalar functions for
    f^\star, then Cov(f_i(x), f_j(x')) = 0.

    The functions returned by sample and mean return an m-dimensional array.

    For now, this belief model is deprecated, as it only represents independent
    responses. In later releases, a better way to compose beliefs will be
    introduced.
    """

    class _MultiFunction():
        #pylint: disable=too-few-public-methods
        def __init__(self, vec_func):
            self.vec_func = vec_func

        def __call__(self, x):
            return np.array([f(x) for f in self.vec_func])


    def __init__(self, belief_models):
        """ Constructor

        Builds a multi-dimensional belief model from several other belief
        models.

        Parameters
        ----------
        belief_models : list of BeliefModel
            A list of belief models
        """

        self.belief_models = belief_models


    def sample(self):
        """ Multi-dimensional sample of functions, each selected
        independently from the underlying belief models.

        Returns
        -------
        ff : python function
            A callable python function that takes an alternative x as an input
            and returns a numpy m dimensional array of numbers, where m is the
            number of belief models. This function is a sample from the space
            of functions measured by the underlying belief models.
        """
        vec_func = [b.sample() for b in self.belief_models]
        return self._MultiFunction(vec_func)

    def mean(self, x):
        """ Multi-dimensionsal mean function

        Returns
        -------
        python function
            A callable python function that takes an alternative x as an input
            and returns a numpy m dimensional array of numbers, where m is the
            number of belief models. This function is the expected function, as
            measured by the underlying belief models, taken independently.
        """

        return [b.mean(x) for b in self.belief_models]

    def covariance(self, x, y):
        raise NotImplementedError('MultiBeliefModel covariance function'
                                  + 'implemented')

    def update(self, x, y, sigma2W):
        """ Bayesian update

        Updates the underlying belief models.

        Parameters
        ----------
        x : experiment(s) that were performed
            The actual type of x depends on the underlying belief models.  It
            is assumed that the belief models can accept the same type of
            alternative.
        y : iterable
            A collection of observations for each belief model. The number of
            observations should equal the number of belief models present. It
            is assumed that each entry is understood by their respective belief
            model.
        sigma2W: iterable collection of numbers
            A collection of measurement noise variances. The number of
            variances should equal the number of belief models present. Each
            entry should be a scalar.

        Returns
        -------
        B_np1 : MultiBeliefModel
            A MultiBeliefModel with the same number of underlying belief models
            as this one. Each of the underlying belief model is a Bayesian
            updated version of the belief models in this object.
        """

        b_y_s = zip(self.belief_models, y, sigma2W)
        updated_belief_models = [b.update(x, yy, s) for b, yy, s in b_y_s]
        return MultiBeliefModel(updated_belief_models)
