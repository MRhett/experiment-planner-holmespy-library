from abc import ABC, abstractmethod
import numpy as np

class NoiseModel(ABC):

    @abstractmethod
    def sample(self, x):
        pass

    @abstractmethod
    def mean(self, x):
        pass

    @abstractmethod
    def var(self, x):
        pass

    def std(self, x):
        return np.sqrt(self.var(x))

    def __call__(self, x):
        return var(x)

    

class ConstantGaussianNoise(NoiseModel):
    def __init__(self, var):
        self.var = var
        self.std = np.sqrt(var)

    def sample(self, x):
        return np.random.normal(0, self.std)

    def var(self, x):
        return self.var

    def std(self, x):
        return self.std
