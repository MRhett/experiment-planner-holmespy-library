# Defines the base optimizer class and some commonly used child classes
from abc import ABC, abstractmethod
import scipy.optimize
import numpy as np
from enum import Enum
from collections import namedtuple

# For parallelization
from multiprocessing import Pool
from datetime import datetime
import math

from holmespy.state import BayesianOptimizationState

# Auxilliary classes
class OptType(Enum):
    MAX = 1
    MIN = 2

OptRes = namedtuple('OptRes', ['opt_f', 'opt_x'])


class Optimizer(ABC):
    """
    Abstract base class for optimizers, specifies the signuature for the
    optimize function """

    @abstractmethod
    def optimize(self, f):
        """ Optimizes a function. 
        
        Optimization options should be implemented through
        constructor or other access functions before calling this function.

        Parameters
        ----------
        f : callable
            The function to be optimized. It should be a function of a single
            argument x

        Returns
        -------
        res : OptRes 
            res.opt_f : float 
                the optimum objective function value
            res.opt_x 
                the optimal input, see specific subclasses for details
        """
        pass


class DiscreteOptimizer(Optimizer):
    """ Exhaustive optimzer over a finite set of alternatives

    Simply iterates over a finite collection of alterantives, calculates the
    objective function value and determines the maximum or minimum value seen.
    """

    def __init__(self, X, opt_type=OptType.MAX):
        """ Constructor

        Accepts domain and optimization type

        Parameters
        ----------
        X : iterable
            Finite set of alternatives to calculate objective function values
        opt_type : OptType, optional
            Indicates whether to maximize or minimize the objective function
        """

        self.X = X
        self.opt_type = opt_type

    def optimize(self, f):
        """ Optimizes f

        Optimizes f over the finite set of alternatives given during
        construction. In the case of ties, returns the first encountered
        maximum value (as ordered by X)

        Parameters
        ----------
        
        f : callable
            the function to optimize. Should be a function that accepts a
            single argument
        """

        # Since we're calling minimize, negate function if we wish to perform a
        # maximization.
        sign = 1 if self.opt_type == OptType.MIN else -1
        min_y = float('inf')
        min_x = None

        for x in self.X:
            res = sign*f(x)
            if(res < min_y):
                min_y = res
                min_x = x

        return OptRes(sign*min_y, min_x)


class ContinuousOptimizer(Optimizer):

    class SignedFunction:
        def __init__(self, sign, f):
            self.sign = sign
            self.f = f

        def __call__(self, x):
            return self.sign*self.f(x.T)

    def __init__(self, X, opt_type = OptType.MAX, num_restarts = 10, 
            num_procs = 1, num_initial_samples = 10, method = 'L-BFGS-B', 
            jac = None, hess = None, hessp = None, constraints = (), 
            tol = None, callback = None, opts = None):

        """Constructor

        Initializes the optimizer with the options required to run optimize.
        Wrapper to scipy.optimize.minimize, and as such, requires several of
        the same parameters. One parameter that is not used is args, which is
        meant to pass additional parameters to the objsective funnction,
        jacobian, and hessian. Instead, use lambda functions to define
        functions that take in a single argument (which may be a vector).
        Bounds are defined by the domain argument.

        Parameters
        ----------

        X : ContinuousAlternativeSet
            The domain overwhich to perform the optimization

        opt_type : OptType, optional
            One of OptType.MIN or OptType.MAX to indicate whether we are
            performing minimization or maximization, respectively. Defaults to
            OptType.MAX

        num_restarts : int, optional
            The number of times to restart the optimization, starting from a
            randomly sampled point in the domain (as implemented in
            X.sample()).  Defaults to 10.

        parallelize: boolean, optional
            A flag to indicate whether the different restarts should
            be done in parallel. Defaults to False.

        method : str or callable, optional
            Optimization algorithm to use. Defaults to 'L-BFGS-B' (see
            scipy.optimize.minimize for details).

        jac : bool or callable, optional
            Jacobian information (see scipy.optimize.minimize for details)

        hess, hessp: callable, optional
            Hessian information (see scipy.optimize.minimize for details)

        constraints: dict or sequence of dict, optional
            Optimization constraints (see scipy.optimize.minimize for details)

        tol: float, optionalA
            Tolerance for termination (see scipy.optimize.minimize for details)

        callback: callable, optional
            Called after each function iteration (see scipy.optimize.minimize
            for details)

        opts: dict, opts
            Solver options, method dependent (see scipy.optimize.minimize for
            details)


        See also
        --------
        scipy.optimize.minimize

        """

        self.X = X
        self.opt_type = opt_type
        self.num_restarts = num_restarts
        self.method = method
        self.jac = jac
        self.hess = hess
        self.hessp = hessp
        self.constraints = constraints
        self.tol = tol
        self.callback = callback
        self.opts = opts
        self.num_procs = num_procs
        self.num_initial_samples = num_initial_samples

    def optimize(self, f):
        """ Optimizes the function f

        Optimizes f per the pre-set options specified in the constructor.

        Parameters
        ----------

        f : callable
            The objective function, which is a callable python function that
            accepts a single argument, which is a row vector corresponding
            to an alternative.

        Returns
        -------
        res : OptRes 
            res.opt_f : float, the optimum objective function value
            res.opt_x : array_like,  the optimal input, as a row vector

        """

        # Since we're calling minimize, negate function if we wish to perform a
        # maximization.
        sign = 1 if self.opt_type == OptType.MIN else -1

        # scipy.optimize will call this function with a column vector. Per our
        # convention, f should accept a row vector. Also multiply by correct
        # sign to turn this into a minimization problem if necessary.
        ff = self.SignedFunction(sign, f)

        if(self.num_procs > 1):
            ## run independent restarts in parallel
            pool = Pool(self.num_procs)
            jobs = [ pool.apply_async(self._optimize, (ff, i, True)) \
                    for i in range(self.num_restarts)]

            res = [ j.get() for j in jobs]
            pool.close()
            pool.join()
        else:
            # loop over restarts, keep track of minimum function
            res = [ self._optimize(ff, i) \
                    for i in range(self.num_restarts) ]

        i_min  = np.argmin([ y[0] for y in res ])
        min_f = res[i_min][0]
        min_x = res[i_min][1]

        return OptRes(sign*min_f, min_x)

    def _optimize(self, f, i, seed=False):
        min_f = float('inf')
        min_x = None
        success = False
        num_tries = 0
        max_num_tries = 10

        # seed rnd if done in parallel
        if(seed):
            now = datetime.now()
            seed = hash((now.second +i) * 1e6 + now.microsecond )
            np.random.seed(seed)

        # keep going until at least one optimization was successful
        while(not success):

            min_x0 = None
            min_f0 = float('inf')
            found = False

            # burn-in for rng
            for j in range(i+1):
                x0 = self.X.sample().T

            # sample a good starting point
            for k in range(self.num_initial_samples):
                x0 = self.X.sample().T
                f0 = f(x0)
                if(f0 < min_f0):
                    found = True
                    min_f0 = f0
                    min_x0 = x0

            if not found:
                continue

            x0 = min_x0

            res = scipy.optimize.minimize(f, x0, 
                    method = self.method,
                    jac = self.jac,
                    hess = self.hess,
                    hessp = self.hessp,
                    constraints = self.constraints,
                    tol = self.tol,
                    callback = self.callback,
                    options = self.opts,
                    bounds = self.X.bounds())

            # save results if needed
            if res.success and res.fun < min_f:
                success = res.success
                min_f = res.fun
                min_x = res.x

            num_tries += 1
            if(num_tries >= max_num_tries):
                break

        return (min_f, min_x)


class BayesianOptimizer:

    def __init__(self, X, aux_optimizer, aux_policy, B0, sigma2W_f, 
            num_iter = 50):
        self.aux_optimizer = aux_optimizer
        self.aux_policy = aux_policy

        self.S0 = BayesianOptimizationState(B0, 0, {
            'experimental_budget': num_iter,
            'actions': X,
            'optimizer': aux_optimizer,
            'noise_variance_f': sigma2W_f
            })

        self.sigma2W_f = sigma2W_f
        self.num_iter = num_iter

    def optimize(self, f):
        S = self.S0

        for _ in range(self.num_iter):
            x = self.aux_policy.suggest(S)
            y = f(x)
            S = S.step(x, y)

        f_N = S.current_beliefs.mean
        return self.aux_optimizer.optimize(f_N)
