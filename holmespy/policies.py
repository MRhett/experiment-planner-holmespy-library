"""
Decision making policies, all deriving from the base abstract class
:class:`Policy`.
"""

# pylint: disable=too-few-public-methods

from abc import ABC, abstractmethod
import numpy as np
from scipy import stats

class Policy(ABC):
    """ The abstract base class for all decision making policies.

    Defines the required signature for the sole class method :func:`suggest`.
    """

    @abstractmethod
    def suggest(self, state):
        """ Suggests an experiment to run.

        Suggests an experiment to run given the :class:`PriorState` state, 
        `state`.

        Args:
            state (:class:`state.PriorState`): The prior state with which an 
                action is selected.

        Returns:
            An action, the type of which depends on the problem.
        """


class ExplorationPolicy(Policy):
    """ Pure exploration.

    A policy that selects a random action from among the prior state's
    available actions.
    """

    def suggest(self, state):
        return state.available_actions().sample()

    def __str__(self):
        return "Explr"

class ExploitationPolicy(Policy):
    """ Pure Exploitation

    Given current beliefs, this policy selects :math:`x^n = \\arg\\max f^n(x)`.
    """

    def __init__(self, optimizer):
        self.optimizer = optimizer

    def suggest(self, state):
        _, max_x = self.optimizer.optimize(state.current_beliefs.mean)
        return max_x

    def __str__(self):
        return "Exploitation"


class MaxVariancePolicy(Policy):
    """ Maximum variance policy

    Selects the action whose corresponding response has the most uncertainty
    associated with it.
    """

    def __init__(self, optimizer):
        self.optimizer = optimizer

    def suggest(self, state):
        _, max_x = self.optimizer.optimize(state.current_beliefs.variance)
        return max_x

    def __str__(self):
        return "MaxVar"


class UCBPolicy(Policy):
    """ Upper Confidence Bound

    Selects the action :math:`x` that maximizes 
    :math:`f^n(x) + c\\cdot\\std^n(x)` for some scale parameter :math:`c`.
    """

    def __init__(self, optimizer, scale=np.sqrt(2)):
        """
        Args:
            optimizer (:class:`optimizers.Optimizer`): The optimizer that
                will be used to select the alternative with maximum UCB value.
            scale (float, optional): The scaling parameter to indicate
                how many standard deviations from the mean estimate to use
                as the UCB value. Defaults to :math:`\\sqrt{2}`.
        """
        self.optimizer = optimizer
        self.scale = scale

    def suggest(self, state):
        utility = self._UCBUtility(state, self.scale)
        _, max_x = self.optimizer.optimize(utility)
        return max_x

    def __str__(self):
        return "UCB_%06.2f"%self.scale


    class _UCBUtility():
        # A callable class representing the UCB utility function.

        def __init__(self, state, scale):
            self.scale = scale
            self.mean_f = state.current_beliefs.mean
            self.std_f = state.current_beliefs.std

        def __call__(self, x):
            return self.mean_f(x) + self.scale*self.std_f(x)


class EIPolicy(Policy):
    """
    The Expected improvement for gaussian beliefs

    Args:
        optimizer (:class:`optimizers.Optimizer`): The optimizer that
            will be used to select the alternative with maximum EI value.
        sigam2W_f (function): The noise variance function that accepts an
            input ``x`` and returns the variance of the (Normally distributed) 
            noise associated with the response for the given input ``x``.

    """

    def __init__(self, optimizer, sigma2W_f):
        self.optimizer = optimizer
        self.sigma2W_f = sigma2W_f

    def suggest(self, state):
        utility = self._EIUtility(state, self.sigma2W_f)
        _, max_x = self.optimizer.optimize(utility)
        return max_x


    class _EIUtility():

        def __init__(self, state, sigma2W_f):
            self.ymax = max(state.hist_outcomes) \
                    if len(state.hist_outcomes) > 0 else np.NINF
            self.mean = state.current_beliefs.mean
            self.var = state.current_beliefs.variance
            self.sigma2W_f = sigma2W_f


        def __call__(self, x):
            if(self.ymax == np.NINF):
                return 0

            mean_x = self.mean(x)
            var_x = self.var(x)
            std_x = np.sqrt(self.sigma2W_f(x) + var_x)
            return (mean_x - self.ymax)*stats.norm.cdf(self.ymax,mean_x,std_x)\
                    + var_x*stats.norm.pdf(self.ymax, mean_x, std_x)


class GeneralizedKGPolicy(Policy):
    """
    The generalized KG policy.

    Given a state distance function d(S, S'), select the action that 
    maximizes the expected value E[d(S, S')]. That is, pick the action with
    the most potential to move away from the current state.

    Args:
        optimizer (:class:`optimizer.Optimizer`): The optimier that will
            be used to maximize the expected value function.
        noise_var_f (function): The noise variance function that accents an 
            input ``x`` and returns the variance of the (Normally distributed)
            noise associated with the respons for the given input ``x``.
        state_vec_f (function): A map from state space to finite dimensional 
            euclidean space.
        num_sims (int): The number of simulations to use to calculate the
            expected value for any input ``x``. Optional, defaults to 20.
    """

    def __init__(self, optimizer, noise_var_f, state_vec_f, num_sims = 20):
        self.optimizer = optimizer
        self.noise_var_f = noise_var_f
        self.state_vec_f = state_vec_f
        self.num_sims = num_sims

    def suggest(self, state):
        utility = self._build_utility_f(state)
        _, max_x = self.optimizer.optimize(utility)
        return max_x

    def _build_utility_f(self, state):
        utility = self._KGUtility(state, self.noise_var_f, self.state_vec_f, 
                self.num_sims)
        return utility

    class _KGUtility:
        def __init__(self, state, noise_var_f, state_vec_f, 
                num_sims):

            self.state = state
            self.noise_var_f = noise_var_f
            self.state_vec_f = state_vec_f
            self.prior_vec = state_vec_f(state)
            self.num_sims = num_sims


        def __call__(self, x):
            sigma2W_x = self.noise_var_f(x)
            mu_x = self.state.current_beliefs.mean(x)
            sigma2x = self.state.current_beliefs.variance(x)
            sigma_y  = np.sqrt(sigma2W_x + sigma2x)

            rng = np.random.normal
            step = self.state.step
            norm = np.linalg.norm
            state_vec = self.state_vec_f

            ret_val = 0
            for _ in range(self.num_sims):
                y = rng(mu_x, sigma_y)
                posterior_vec = state_vec(step(x, y))
                ret_val += norm(posterior_vec - self.prior_vec)

            return ret_val/self.num_sims


class MCTSPolicy(Policy):
    """ Monte Carlo Tree Search policy for stochastic outcomes.

    This implements the Monte Carlo Tree Search policy for problems with
    stochastic outcomes. Implements

    """

    def __init__(self, params):
        """
        Args:
            params (:class:`MCTSParameters`): The set of parameters describing
                auxillary policies for use in MCTS
        """
        self.params = params
        self.root = None

    def __str__(self):
        return "MCTS_%s" % self.params

    def suggest(self, state):
        self.root = MCTSPolicy._MCTSPriorNode(state, None, self.params)

        # iteratively add nodes into the tree and update value functionst
        for _ in range(self.params.comp_budget):

            # add (or resample) node
            new_node = self.__select_and_expand()

            # calculate simulated value of the node
            self.__rollout(new_node)

            # propogate value up through tree
            MCTSPolicy.__backup(new_node.parent)

        # pick the best action according to final selection policy
        best_child = self.params.final_selection_policy.select(self.root)

        return best_child.action



    def __select_and_expand(self):
        # Select/Expansion step of MCTS. Traverses scenario tree until it finds
        # an available position for a new prior node.
        #
        # Returns: 
        #     :class:`MCTSPriorNode`: A node that was just added to the 
        #         scenario tree using the current tree policy.
        #
        # Todo:
        #   What to do when the traversal ends in a terminal state. 

        curr = self.root
        while not curr.state.is_terminal():
            # select or create a post decision state
            if curr.is_expandable():
                curr_pd = curr.expand()
            else:
                curr_pd = self.params.prior_selection_policy.select(curr)

            # select or create a prior state
            if curr_pd.is_expandable():
                return curr_pd.expand()
            curr = self.params.postdecision_selection_policy.select(curr_pd)

        return curr

    
    def __rollout(self, node):
        # Determines the initial value for the value of the node v by
        # performing a roll-out simulation until a terminal state is reached.
        # This will change ``node.value`` and ``node.num_sims``.
        #
        # Args:
        #     node (_MCTSPriorNode): An _MCTSPriorNode instanceA
        #
        # Todo:
        #     cumulative rewards

        # the current node starts to simulate MCTS
        state = node.state
        # The truth that we're using for simulations
#        f_truth = state.current_beliefs.sample_f()

        # the current node is not terminal
        while not state.is_terminal():
            # pick an action at random
            action = self.params.simulation_policy.suggest(state)

            # sample a transition
#            state = state.step(action)

            # TODO: do we use joint distribution, or conditional on f_truth?
            sigma2W_x = self.params.sigma2W(action)
            #mu_x = f_truth(action)
            mu_x = state.current_beliefs.mean(action)
            sigma2x = state.current_beliefs.variance(action)
            sigma_y  = np.sqrt(sigma2W_x + sigma2x)
            outcome = np.random.normal(mu_x, sigma_y)
            state = state.step(action, outcome)

        # collect value observed for this simulation
        # (right now only final rewads)
        obs_value = state.final_reward()

        # final reward is used for value, averaged wth past values
        node.value = (node.num_sims*node.value + obs_value) \
                / (node.num_sims + 1)

        # increment simulation count
        node.num_sims += 1

    @staticmethod
    def __backup(node):
        # Backpropogates value for a node and all of its ancesstors.
        #
        # Args:
        #   node: The node who's value has been updated, which requires the 
        #       value of its ancestors to be subsequently updated as well.

        while node is not None:
            # increment simulation count
            node.num_sims += 1

            # update value function
            node.calculate_value()

            # go up one level
            node = node.parent


    class _MCTSNode(ABC):
        # The tree/node data structure for use in stochastic MCTS. An MCTS
        # Scenario tree is a tree comprising of both Prior and Post-Decision
        # states. Each node holds a state (either a PriorState or PostDecision
        # state), a number N representing the number of paths passing through
        # the node, a number Q that represents the sum of all its child values.

        # It also holds a reference to its parent node (if it exists) and a list
        # of references to its children.

        # By convention, any data associated to an edge between A and B:
        #
        #        d
        #     A ---> B
        #
        # is stored in B. Examples of this data include single period reward,
        # outcomes (if A is a post-decision state and B is a posterior/prior
        # state), and selected actions (if A is prior state and B is a
        # post-decision state).

        

        def __init__(self, state, parent, params):
            """
            Args:
                state (:class:`State`): The st ate this node is a wrapper for.
                parent (:class:`_MCTSNode`): The parent of this node in the
                    scenario tree. If this node is a :class:`_MCTSPriorNode`,
                    then the parent must be a :class:`_MCTSPostDecisionNode`,
                    and vice versa.
                params (:class`MCTSParameters`): MCTS parameters.
            """
            self.state = state
            self.parent = parent
            self.children = []
            self.params = params
            self.num_children = 0
            self.value = 0
            self.num_sims = 0

        def add_child(self, child):
            """
            Adds a child node.

            Parameters
            ---------
            :param child: The child to add
            :type child: either a :class:`_MCTSPriorNode` or
            :class:`_PosteriorNode`, this should alternative between parent and
            child.
            """

            self.children.append(child)
            self.num_children += 1

        @abstractmethod
        def is_expandable(self):
            """
            Determines if this node can have more children, based on
            whatever rules are implemented for the tree.

            Return
            ------
            :return: `bool, `True` if the node can have more children, `False`
                otherwise
            """

        @abstractmethod
        def expand(self):
            """
            Adds a child node to this node
            """

        @abstractmethod
        def calculate_value(self):
            """
            Aggregrates value of children.
            """

    class _MCTSPriorNode(_MCTSNode):
        """
        Represents a node holding a prior state.  Prior states always have
        parents that are either None (for the root node) or of type
        :class:`__MCTSPostDecisionNode`. All its children will also be
        :class:`__MCTSPostDecisionNode`s.  Data associated to this node come
        from the transition from the parent PostDecisionNode, in particular an
        outcome, and possibly a single period reward.
        """

        def __init__(self, state, parent, params):
            # call base class constructor
            super().__init__(state, parent, params)

            # maintain a list of untried actions, initially populate with all
            # available actions for the state
            self.untried_actions = state.available_actions()


        def add_child(self, child):
            """"
            Overrides `add_child` in :class:`_MCTSNode`
            """

            super().add_child(child)

            # update untried actions to remove the action associated to the
            # child post-decision state
            self.untried_actions = self.untried_actions.remove(child.action)


        def is_expandable(self):
            """
            Implements `is_expandable` from :class:`_MCTSNode`.
            """
            return (self.untried_actions.size() > 0) and \
                self.params.prior_threshold_satisfied(self)


        def expand(self):
            """
            Implements `expand` from :class:`_MCTSNode`.
            """
            # uniform-randomly sample an action
            # TODO: action sampling policy
            action = self.untried_actions.sample()

            # create a new post decision node
            post_decision_node = \
                    MCTSPolicy._MCTSPostDecisionNode(self.state, self, 
                                                     self.params, action)
            # add the child to this node
            self.add_child(post_decision_node)

            return post_decision_node

        def calculate_value(self):
            child_vals = [c.value for c in self.children]

            if self.params.expectimax:
                self.value = max(child_vals)
            else:
                self.value = np.mean(child_vals)


    class _MCTSPostDecisionNode(_MCTSNode):
        """
        Represents a node holding a post-decision state S^n_x. Post-decision
        states always have parents that are of type :class:`MCTSPriorNode`
        (`None` parents are not allowed since an MCTS Scenario tree must always
        be rooted at a prior state). All its children must also be type
        :class:`MCTSPriorNode`.  Data associated to this node comes from the
        transition from the parent Prior Node. In particular, an action.  We
        assume no rewards come from making a decision.
        """

        def __init__(self, state, parent, params, action):
            # call base class constructor
            super().__init__(state, parent, params)

            sigma2W_x = params.sigma2W(action)
            self.mu = state.current_beliefs.mean(action)
            sigma2x = state.current_beliefs.variance(action)
            self.sigma  = np.sqrt(sigma2W_x + sigma2x)

            # save action
            self.action = action

        def is_expandable(self):
            return self.params.postdecision_threshold_satisfied(self)

        def expand(self):
            # TODO: We may want to sample posterior states differently
            # than what's prescribed in the state's step function. For
            # example, we may want to do some importance sampling.

            outcome = np.random.normal(self.mu, self.sigma)
            posterior_state = self.state.step(self.action, outcome)

            # TODO: single period rewards.
            posterior_node = \
                    MCTSPolicy._MCTSPriorNode(posterior_state, self,
                                              self.params)

            # add the child to this node
            self.add_child(posterior_node)

            return posterior_node

        def calculate_value(self):
            self.value = np.mean([c.value for c in self.children])


    class MCTSParameters():
        """
        A container class that holds several parameters needed for MCTS

        :param comp_budget: The number of nodes to add to the scenario tree
            before making a decision.
        :type comp_budget: `int`, positive
        :param sigma2W: The noise model for simulations
        :type sigma2W: a callable python function
        :param k_prior_thresh: The prefactor for the DPW threshold used in
            determining whether to expand from a prior state. Larger values
            means prior states will have more child nodes (more actions will
            be included in simulations). Optional, default value = 1.
        :type k_prior_thresh: ``float``
        :param alpha_prior_thresh: The exponent for the DPW threshold used
            determining whether to expand from a prior state. Larger values
            means prior states will have more child nodes Optional, default
            value = 0.5.
        :type alpha_prior_thresh: ``float``

        :param k_postdecision_thresh: The prefactor for the DPW threshold used
            in determining whether to expand from a postdecision state. Larger
            values means postdecision states will have more child nodes (more
            outcomes will be included in simulations). Optional, default value
            = 1.
        :type k_postdecision_thresh: ``float``

        :param alpha_postdecision_thresh: The exponent for the DPW threshold
            used determining whether to expand from a postdecision state.
            Larger values means postdecison states will have more child nodes.
            Optional, value = 0.5.
        :type alpha_postdecision_thresh: ``float``

        :param simulation_policy: The policy used in during the roll-out
            simulation
        :type simulation_policy: :class:`Policy`
        :param prior_selection_policy: The tree policy used to select a child
            node of a prior node
        :type prior_selection_policy: :class:`MCTSTreePolicy`
        :param postdecision_selection_policy: The tree policy used to select a
            child node of a postdecision node
        :type postdecision_selection_policy: :class:`MCTSTreePolicy`
        :param final_selection_policy: The tree policy used to select a child of
            the root prior node as the selected action of the policy.
        :type final_selection_policy: :class:`MCTSTreePolicy`
        """

        # pylint: disable=too-many-instance-attributes
        # pylint: disable=too-many-arguments

        def __init__(self, comp_budget, sigma2W,
                     k_prior_thresh=1, alpha_prior_thresh=0.5,
                     k_postdecision_thresh=1, alpha_postdecision_thresh=0.5,
                     simulation_policy=None,
                     prior_selection_policy=None,
                     postdecision_selection_policy=None,
                     final_selection_policy=None,
                     expectimax=False):

            self.comp_budget = comp_budget
            self.sigma2W = sigma2W
            self.expectimax = expectimax

            self.k_prior_thresh = k_prior_thresh
            self.alpha_prior_thresh = alpha_prior_thresh
            self.k_postdecision_thresh = k_postdecision_thresh
            self.alpha_postdecision_thresh = alpha_postdecision_thresh

            # Auxilliary policies
            if simulation_policy is None:
                self.simulation_policy = ExplorationPolicy()
            else:
                self.simulation_policy = simulation_policy

            if prior_selection_policy is None:
                self.prior_selection_policy = MCTSPolicy.UCTPolicy()
            else:
                self.prior_selection_policy = prior_selection_policy

            if postdecision_selection_policy is None:
                self.postdecision_selection_policy = MCTSPolicy.UCTPolicy()
            else:
                self.postdecision_selection_policy = \
                        postdecision_selection_policy

            if final_selection_policy is None:
                self.final_selection_policy = \
                        MCTSPolicy.ExploitationTreePolicy()
            else:
                self.final_selection_policy = final_selection_policy

        def prior_threshold_satisfied(self, node):
            """
            Determines whether the prior node ``node`` is expandable.
            """

            return node.num_children <= self.k_prior_thresh \
                    * np.power(node.num_sims, self.alpha_prior_thresh)

        def postdecision_threshold_satisfied(self, node):
            """
            Determines whether the postdecision  node ``node`` is expandable.
            """

            return node.num_children <= self.k_postdecision_thresh \
                    * np.power(node.num_sims, self.alpha_postdecision_thresh)

        def __str__(self):
            return "%04d_%05.2f_%04.2f_%05.2f_%04.2f_%s_%s_%s_%s_%s" % (
                self.comp_budget,
                self.k_prior_thresh,
                self.alpha_prior_thresh,
                self.k_postdecision_thresh,
                self.alpha_postdecision_thresh,
                self.simulation_policy,
                self.prior_selection_policy,
                self.postdecision_selection_policy,
                self.final_selection_policy,
                self.expectimax)


    ##
    # Tree Policy Implementations
    ##

    class MCTSTreePolicy(ABC):
        """
        Given an MCTSNode node, a MCTS Tree policy selects a child of that
        node.
        """

        @abstractmethod
        def select(self, node):
            """
            Selects a child of the :class:`MCTSNode` ``node``.

            :param node: The node from which we are selecting a child
            :type node: :class:`MCTSNode`
            """


    class UCTPolicy(MCTSTreePolicy):
        """
        Selects an child node according the the UCB-Tree policy
        """

        def __init__(self, scale=2):
            self.scale = scale

        def select(self, node):
            scale2 = self.scale*np.sqrt(np.log(node.num_sims))
            best_val = -np.inf
            best_child = None

            for child in node.children:
                val = child.value + scale2*np.sqrt(1.0/child.num_sims)
                if val > best_val:
                    best_val = val
                    best_child = child

            return best_child

        def __str__(self):
            return "UCT_%06.2f" % self.scale


    class ExploitationTreePolicy(MCTSTreePolicy):
        """
        Selects the child node with maximum value
        """

        def select(self, node):
            # calculate values
            best_val = -np.inf
            best_child = None

            for child in node.children:
                val = child.value
                if val > best_val:
                    best_val = val
                    best_child = child

            return best_child

        def __str__(self):
            return "EXPT"

    class MaxNumSimsPolicy(MCTSTreePolicy):
        """
        Selects the child with highest number of simulations.
        """

        def select(self, node):
            i_max = np.argmax([child.N for child in node.children])
            return node.children[i_max]

        def __str__(self):
            return "MNS"


    class SampleByNumSimsPolicy(MCTSTreePolicy):
        """
        Selects the child by sampling, with probability proportional to
        the number of simulations each child has participated in.
        """

        def select(self, node):
            probs = np.array([child.num_sims for child in node.children])
            probs = probs / np.sum(probs)
            return np.random.choice(node.children, p=probs)

        def __str__(self):
            return "SBNS"
