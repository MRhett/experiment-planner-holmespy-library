"""
Generic simulator for MCTS, MCDP

Future Requirements:
1. Should keep around all the information about the simulation, including
state, action outcome, noise, and ground truth.
2. It should be easy to pick-up and continue a simulation. For example:
    * If the computer crashes, we should be able to use a snapshot file to 
      re-start the simulation
    * Extend the simulation by adding
        * new simulations (increase NUM_SIMS)
        * new experiments (increasing NUM_EXPS)
        * adding new policies
        * new noise levels
3. It should do some bookkeeping regarding the version of code that was used 
including:
    * All problem specific model code
    * MCTS/Holmespy code
    * The simulation code itself (this file)

4. Should be able to vary noise, and internal parameters, but use the same 
truths (if truths are not affected parameters) and measurements (if not 
affected by parameters).

"""

from enum import Enum
import numpy as np
import pickle
import bz2

class Simulation:
    """ Represents a single simulation, from starting to terminal state.
    """

    class SimStatus(Enum):
        """ The status of a simulation
        An enum class enumerating status as either SimStatus.INITIALIZED, 
        .RUNNING, and .COMPLETE
        """
        INITIALIZED = 0
        RUNNING = 1
        COMPLETE = 2

    class SimState:
        """ A simulation state, consisiting of action, outcome and posterior 
        state
        """
        def __init__(self, a_n, y_np1, state_np1):
            self.a_n = a_n
            self.y_np1 = y_np1
            self.state_np1 = state_np1

    def __init__(self, f_star, params):
        self.params = params
        self.f_star = f_star
        self.status = Simulation.SimStatus.INITIALIZED
        self.sim_states = []

    def run(self):
        """ Runs the simulation
        """
        self.status = Simulation.SimStatus.RUNNING

        S_n = self.params.S_0
        while not S_n.is_terminal():
            # select action
            a_n = self.params.policy.suggest(S_n)

            # noisy observation
            y_np1 = self.f_star(a_n) + \
                    np.random.normal(0, np.sqrt(self.params.sigma2W(a_n)))

            # Transition function
            S_n = S_n.step(a_n, y_np1)

            # save the simulation state
            self.__add_sim_state(Simulation.SimState(a_n, y_np1, S_n))

        self.status = Simulation.SimStatus.COMPLETE

    def __add_sim_state(self, sim_state):
        self.sim_states.append(sim_state)

    def save(self, file_name):
        with bz2.BZ2File(file_name, 'wb') as f:
            pickle.dump(self, f)

    def load(file_name):
        with bz2.BZ2File(file_name, 'rb') as f:
            return pickle.load(f)


class SimulationParameters:
    def __init__(self, S_0, policy, sigma2W):
        self.S_0 = S_0
        self.policy = policy
        self.sigma2W = sigma2W

    def save(self, file_name):
        pickle.dump(self, open(file_name, "wb"))

    def load(file_name):
        return pickle.load(open(file_name, "rb"))


class Simulator:
    """ Simulator for MCTS
    Every call to .simulate runs on the same set of ground truths.
    """
    def __init__(self, num_sims, B_truth, verbose = False):
        """
        :param num_sims: The number of simulations to perform
        :type num_sims: int, positive
        :param B_truth: a prior model on ground truth from which we sample 
        truths
        :type B_truth: :class:`holmespy.beliefmodels.BeliefModel`
        """

        self.num_sims = num_sims
        self.B_truth = B_truth
        self.verbose = verbose

        # Generate truths
        self.f_truths = [B_truth.sample_f() for i in range(num_sims)]

    def save(self, file_name):
        with bz2.BZ2File(file_name, 'wb') as f:
            pickle.dump(self, f)

    def load(file_name):
        with bz2.BZ2File(file_name, 'rb') as f:
            return pickle.load(f)

    def simulate(self, params, start=0, end=None):
        """ Runs several simulations over the same set of ground truths.
        """
        simulations = []

        if(end == None) or (end > self.num_sims):
            end = self.num_sims
        truths = self.f_truths[start:end]

        for (i, f_truth) in enumerate(truths):

            if(self.verbose):
                print("\tSimulation %d : [%d, %d]" % (i+start, start, end))

            # This for-loop can be parallelized
            simulation = Simulation(f_truth, params)
            simulation.run()
            simulations.append(simulation)

        return simulations


def calculate_oc(simulations, optimizer):
    """
    Calculates OC at each step for the list of simulations provided
    :param simulations: The list of simulations
    :type simulations: ```list`` of :class:`Simulation`
    :return a ``list`` of ``lists`` containing oc values for each step in each 
        simulation:
    """

    oc = []

    # iterate over the NUM_SIM simulations
    for j, sim in enumerate(simulations):
        S = sim.params.S_0
        f_star = sim.f_star
        y_star,_ = optimizer.optimize(f_star)

        oc_j = []

        # iteratove over NUM_EXPERIMENTS experiments
        for n, sim_state in enumerate(sim.sim_states):
            # calculate relative opportunity cost
            oc_j.append(_calculate_oc(S, f_star, y_star, optimizer))

            # step
            S = sim_state.state_np1

        # final OC
        oc_j.append(_calculate_oc(S, f_star, y_star, optimizer))
        oc.append(oc_j)

    return oc


def _calculate_oc(S, f_star, y_star, optimizer):
    # f_n = current estimate
    f_n = S.current_beliefs.mean

    # the best experiment according to current beliefs
    _, x_star_n = optimizer.optimize(f_n)

    # the true value at the suggested best experiment
    y_star_n = f_star(x_star_n)

    # calculate relative opportunity cost
    return np.abs((y_star - y_star_n)/y_star)


