from abc import ABC, abstractmethod
import numpy as np

class State(ABC):
    """An abstract MDP state class."""

    @abstractmethod
    def is_terminal(self):
        """
        Returns true if this prior state is terminal. A state is
        terminal if no actions can be taken from it
        """

    @abstractmethod
    def single_period_reward(self, posterior_state):
        """Calculate the reward obtained from a transition from this state.

        Given a posterior state :param:`poster_state`, calculate the
        single-period reward (if any) obtained from  transitiong from this
        state to the posterior state.

        Returns:
            float: The single period reward.
        """

    @abstractmethod
    def final_reward(self):
        """Calculate the reward obtained from terminating at this state.

        The final reward defines the reward (if anything) for arriving at this
        state at the end of the decision process. 

        Returns:
            float: The final reward
        """

    @abstractmethod
    def available_actions(self):
        """
        Returns the list of actions that can be taken from this state, assuming
        it is a prior state. This list may be altered in the implementation of
        MCTS, so its important to return a deep copy of the list

        :return: A :class:`AlternativeSet` representing available actions
        """

    @abstractmethod
    def step(self, action, outcome):
        """Apply transition from this state for the given action, and observed
        outocme"

        Given the acation :param:`action` and stochastic outcome :y:, 
        apply an MDP transition function.  

        Returns:
            State: A posterior state obtained from transitioning from this state
                with the provided action.
        """


class BayesianOptimizationState(State):
    """ Belief state for response functions, for Bayesian Optimization.

    In Bayesian Optimization, we assume the state consists solely of a 
    state of belief. We also assume:

    * There is an explicit experimental budget,
    * All possible actions are available within any state
    * Final rewards are solely a function of the terminal state, and
      is calculated to maximize some transformation of the final mean estimate
    * There are no single-period rewards.

    To use this kind of state, you must provide the noise model, which is a
    callable function ``f(a)`` that takes in a single argument ``a``
    representing an action , and returns the noise variance of the response.

    Additionally, you must provide the experimental budget, and set of possible
    actions. These are all specified in a dictionary:

    .. code-block:: python
       params = {
           'experimental_budget': 20,
           'actions': my_actions,
           'optimizer': optimizer,
           'noise_variance_f': my_noise_variance
       }
    
    """

    # TODO: require noise_std instead of variance
    # TODO: Factor out historical data.
    
    def __init__(self, current_beliefs, num_exps, params,
            hist_actions = None, hist_outcomes = None):
        """
        Args:
            current_beliefs (:class:`BeliefModel`): The time-n beliefs this 
                state represents.
            num_exps (int): The number of experiments already performed.
            params (dict): A python dictionary containing values for 
                ``experimental_budget``, ``actions``, ``optimizer``, and
                ``noise_varinace_f``.
            hist_actions (list): A list of all the inputs x_n run so far,
            hist_outcomes (list): A list of all the outputs  y_{n+1} observed 
                so far,
        """

        self.current_beliefs = current_beliefs
        self.num_exps = num_exps
        self.params = params

        if hist_actions is None:
            self.hist_actions = []
        else:
            self.hist_actions = hist_actions

        if hist_outcomes is None:
            self.hist_outcomes = []
        else:
            self.hist_outcomes = hist_outcomes


    def is_terminal(self):
        return self.num_exps == self.params['experimental_budget']


    def available_actions(self):
        return self.params['actions']


    def step(self, action, outcome):
        sigma2W = self.params['noise_variance_f'](action)
        posterior = self.current_beliefs.update(action, outcome, sigma2W)

        hist_actions = self.hist_actions.copy()
        hist_outcomes = self.hist_outcomes.copy()
        hist_actions.append(action)
        hist_outcomes.append(outcome)

        return BayesianOptimizationState(\
                posterior, self.num_exps + 1, self.params,
                hist_actions, hist_outcomes)

    def single_period_reward(self, posterior_state):
        return 0

    def final_reward(self):
        return self.params['optimizer'].optimize(self.current_beliefs.mean)[1]

