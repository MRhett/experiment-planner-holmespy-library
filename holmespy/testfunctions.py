"""
This module contains some benchmark test functions and prior builder that
can be used to asses the performance of any custom models or policies.
"""

import numpy as np
from holmespy.alternativesets import DiscreteAlternativeSet
from holmespy.beliefmodels import LookupTableBeliefModel

class Rosenbrock2DSampler():
    class _RosenbrockFunction():
        def __init__(self, a, b, c):
            self.a = a
            self.b = b
            self.c = c

        def __call__(self, x):
            return self.c*(self.a-x[0])**2 + self.b*(x[1] - x[0]**2)**2

    def __init__(self, a_lim, b_lim, c_lim):
        self.a_lim = a_lim
        self.b_lim = b_lim
        self.c_lim = c_lim

    def sample_f(self):
        a, b, c = _unif_sample(self.a_lim, self.b_lim, self.c_lim)
        return Rosenbrock2DSampler._RosenbrockFunction(a, b, c)

class Quadratic1DSampler():
    class _QuadraticFunction():
        def __init__(self, r1, r2, a):
            self.r1 = r1 
            self.r2 = r2
            self.a = a

        def __call__(self, x):
            return self.a*(x - self.r1)*(x - self.r2)

    def __init__(self, r1_lim, r2_lim, a_lim):
        self.r1_lim = r1_lim
        self.r2_lim = r2_lim
        self.a_lim = a_lim

    def sample_f(self):
        a, r1, r2 = _unif_sample(self.a_lim, self.r1_lim, self.r2_lim)
        return Quadratic1DSampler._QuadraticFunction(r1, r2, a)

class Logistic1DSampler():
    class _LogisticFunction():
        def __init__(self, k, x0):
            self.k = k
            self.x0 = x0

        def __call__(self, x):
            return 1.0/(1.0 + np.exp(-self.k*(x-self.x0)))

    def __init__(self, k_lim, x0_lim):
        self.k_lim = k_lim
        self.x0_lim = x0_lim

    def sample_f(self):
        k, x0 = _unif_sample(self.k_lim, self.x0_lim)
        return Logistic1DSampler._LogisticFunction(k, x0)

class Linear1DSampler():
    class _LinearFunction():
        def __init__(self, m, b):
            self.m = m
            self.b = b

        def __call__(self, x):
            return self.b + self.m*x

    def __init__(self, m_lim, b_lim):
        self.b_lim = b_lim
        self.m_lim = m_lim

    def sample_f(self):
        m, b = _unif_sample(self.m_lim, self.b_lim)
        return Linear1DSampler._LinearFunction(m, b)


class LookupTablePriorGenerator():
    def __init__(self, X):
        self.X = X
        assert(isinstance(X, DiscreteAlternativeSet))

    def generate_from_sampler(self, sampler, num_samples = 1000):
        samples = self._generate_samples(sampler, num_samples)
        mu = np.mean(samples, axis = 0)
        Sigma = np.cov(samples, rowvar=False)
        # Add small amount to make full rank
        Sigma = Sigma + 1e-4*np.identity(len(mu))
        return LookupTableBeliefModel(mu, Sigma, self.X)

    def _generate_samples(self, sampler, num_samples):
        ret_val = np.zeros([num_samples, self.X.size()])
        for n in range(num_samples):
            f = sampler.sample_f()
            ret_val[n,:] = np.array([f(x) for x in self.X])
        return ret_val


    def generate_from_kernel(self, mu_0, sigma2, l):
        mu = mu_0*np.ones(self.X.size())
        Sigma = np.zeros([self.X.size(), self.X.size()])

        for i, x in enumerate(self.X):
            xx = np.array(x)
            for j, y in enumerate(self.X):
                yy = np.array(y)
                Sigma[i,j] = sigma2 * np.exp(-np.linalg.norm(xx - yy)**2 / l**2)
        return LookupTableBeliefModel(mu, Sigma, self.X)

def _unif_sample(*lim):
    if(len(lim) < 2):
        l = lim[0]
        return np.random.random()*(l[1] - l[0]) + l[0]
    else:
        return (np.random.random()*(l[1] - l[0]) + l[0] for l in lim)
