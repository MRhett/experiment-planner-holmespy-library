#!/usr/bin/env python

from distutils.core import setup

setup(name='holmespy',
      version='0.1.1',
      description='Decision-making under uncertainty',
      author='CSMS Lab',
      author_email='kris@csms.io',
      url='https://bitbucket.org/csmslab/holmespy/',
      packages=['holmespy'],
      install_requires=['numpy', 'matplotlib', 'scipy', 'scikit-learn'])
