import unittest
import numpy as np

from holmespy.beliefmodels import LinearBeliefModel


class LinearBeliefModelTests(unittest.TestCase):

    def setUp(self):
        self.mu_0 = np.random.random(size=5)
        Sigma = np.random.random(size=[5,5])
        self.Sigma_0 = Sigma + Sigma.T

        self.B = LinearBeliefModel(self.mu_0, self.Sigma_0)

        self.basis_functions = [lambda x: x**i for i in range(len(self.mu_0))]
        self.B_wb = LinearBeliefModel(self.mu_0, self.Sigma_0, 
            basis_functions = self.basis_functions)

    def test_mean1(self):
        x = np.random.random(size=5)
        y = self.B.mean(x)
        self.assertEqual(y, np.dot(self.mu_0, x))

    def test_mean2(self):
        x = 1
        y = self.B_wb.mean(x)
        phi = [f(x) for f in self.basis_functions]
        self.assertEqual(y, np.dot(self.mu_0, phi))

    def test_mean3(self):
        # if x doesn't have the right dimensions
        try:
            x = np.random.random(size = 6)
            y = self.B.mean(x)
            # fail test if no error
            self.assertTrue(False)
        except ValueError:
            pass
        except:
            # fail test for any other error
            self.assertTrue(False)

        # if x is a scalar, np may try to be smart...
        try:
            x = np.random.random(size = 1)
            y = self.B.mean(x)
            # fail test if no error
            self.assertTrue(False)
        except ValueError:
            pass
        except:
            # fail test for any other error
            self.assertTrue(False)

    def test_wrong_number_of_basis_functions(self):
        try:
            bad_basis = [lambda x: x**i for i in range(len(self.mu_0)-1)]
            B = linearBeliefModel(self.mu_0, self.Sigma_0, 
                basis_functions = bad_basis)
            # fail test if no error
            self.assertTrue(False)
        except:
            pass

if __name__ == '__main__':
    unittest.main()

