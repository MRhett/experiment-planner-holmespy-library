import unittest
from holmespy.optimizers import DiscreteOptimizer, ContinuousOptimizer, OptType
from holmespy.alternativesets import ContinuousAlternativeSet


class TestOptimizers(unittest.TestCase):

    def test_discrete_optimizer(self):
        X = range(10)
        f = lambda x: (x-3)**2

        max_optimizer = DiscreteOptimizer(X)
        res = max_optimizer.optimize(f)
        self.assertEqual(res.opt_x, 9)
        self.assertEqual(res.opt_f, 36)

        min_optimizer = DiscreteOptimizer(X, OptType.MIN)
        res = min_optimizer.optimize(f)
        self.assertEqual(res.opt_x, 3)
        self.assertEqual(res.opt_f, 0)

    def test_continuous_optimizer_scalar_input_out_of_bounds(self):
        bounds = [(0, 10)]
        X = ContinuousAlternativeSet(bounds)
        f = lambda x: -1.0*(x[0]-12)**2 + 2
        optimizer = ContinuousOptimizer(X)
        res = optimizer.optimize(f)
        self.assertAlmostEqual(res.opt_x[0], 10, places = 6)
        self.assertAlmostEqual(res.opt_f, -2, places = 6)

    def test_continuous_optimizer_scalar_input_in_bounds(self):
        bounds = [(0, 10)]
        X = ContinuousAlternativeSet(bounds)
        f = lambda x: -1.0*(x[0]-3)**2 + 2
        optimizer = ContinuousOptimizer(X)
        res = optimizer.optimize(f)
        self.assertAlmostEqual(res.opt_x[0], 3, places = 6)
        self.assertAlmostEqual(res.opt_f, 2, places = 6)

    def test_continuous_optimizer_scalar_input_minimization(self):
        bounds = [(0, 10)]
        X = ContinuousAlternativeSet(bounds)
        f = lambda x: 1.0*(x[0]-3)**2 + 2
        optimizer = ContinuousOptimizer(X, opt_type=OptType.MIN)
        res = optimizer.optimize(f)
        self.assertAlmostEqual(res.opt_x[0], 3, places = 6)
        self.assertAlmostEqual(res.opt_f, 2, places = 6)

    def test_continuous_optimizer_multivariate_input_in_bounds(self):
        bounds = [(0, 10), (-7, -1)]
        X = ContinuousAlternativeSet(bounds)
        optimizer = ContinuousOptimizer(X)
        f = lambda x: -1.0*( ((x[0] - 3)**2 + (x[1]+5)**2) )
        res = optimizer.optimize(f)
        x = res.opt_x
        y = res.opt_f
        self.assertEqual(x.shape, (2,))
        self.assertAlmostEqual(x[0], 3, places = 6)
        self.assertAlmostEqual(x[1], -5, places = 6)
        self.assertAlmostEqual(y, 0, places = 6)

    def test_continuous_optimizer_multivariate_input_out_of_bounds(self):
        bounds = [(0, 10), (-7, -1)]
        X = ContinuousAlternativeSet(bounds)
        optimizer = ContinuousOptimizer(X)
        f = lambda x: -1.0*( ((x[0] - 12)**2 + (x[1]+5)**2) )
        res = optimizer.optimize(f)
        x = res.opt_x
        y = res.opt_f
        self.assertEqual(x.shape, (2,))
        self.assertAlmostEqual(x[0], 10, places = 6)
        self.assertAlmostEqual(x[1], -5, places = 6)
        self.assertAlmostEqual(y, -4, places = 6)

    def test_continuous_optimizer_parallelization(self):
        bounds = [(0, 10)]
        X = ContinuousAlternativeSet(bounds)
        f = self.ObjectiveFunction()
        optimizer = ContinuousOptimizer(X, opt_type=OptType.MIN, num_procs = 4)
        res = optimizer.optimize(f)
        self.assertAlmostEqual(res.opt_x[0], 3, places = 6)
        self.assertAlmostEqual(res.opt_f, 2, places = 6)


    class ObjectiveFunction:
        def __call__(self, x):
            return 1.0*(x[0]-3)**2 + 2


if __name__ == '__main__':
    unittest.main()
