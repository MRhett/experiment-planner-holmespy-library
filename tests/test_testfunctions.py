import unittest
import numpy as np

from holmespy.alternativesets import DiscreteAlternativeSet

from holmespy.testfunctions import Rosenbrock2DSampler, \
    Quadratic1DSampler, Logistic1DSampler, Linear1DSampler, \
    LookupTablePriorGenerator, _unif_sample


class TestTestFunctions(unittest.TestCase):
    def __test_function(self, f, X, Y, eps = 6):
        for (x,y) in zip(X,Y):
            yy = f(x)
            self.assertAlmostEqual(y, yy, eps)

    def test_rosenbrock2d_function(self):
        f = Rosenbrock2DSampler._RosenbrockFunction(1, 10, 1)
        X = [[1,1], [10,1], [5,1], [1,5]]
        Y = [0, 98091, 5776, 160]
        self.__test_function(f, X, Y)


    def test_quadratic1d_function(self):
        f = Quadratic1DSampler._QuadraticFunction(-2, 2, 1)
        X = [-2, 2, 0, 3, 10, 1]
        Y = [0, 0, -4, 5, 96, -3]
        self.__test_function(f, X, Y)

    def test_logistic1d_sampler(self):
        f = Logistic1DSampler._LogisticFunction(1, 0)
        X = [5, 1, 0, -1, -5]
        Y = [0.99330714907, 0.73105857863, 0.5, 
                0.26894142137, 0.00669285092]
        self.__test_function(f, X, Y)

    def test_lookuptable_prior_generator(self):
        """
        Test lookuptable prior generation from samplers:
            Constant function sampler
        """
        N = 10000
        sampler = Linear1DSampler([-1, 1], [0,1])
        X = DiscreteAlternativeSet([0, 1, 2, 3])
        prior_gen = LookupTablePriorGenerator(X)
        B0 = prior_gen.generate_from_sampler(sampler, num_samples = N)
        f_0 = B0.mean_f()

        # intercept should be within 3 standard deviations of 0.5
        sig_mu =  3.0/np.sqrt(12*N)
        self.assertTrue(np.abs(f_0(0) - 0.5) < sig_mu)

    def test_unif_sample(self):
        N = 100000
        samples = np.zeros(N)
        for i in range(len(samples)):
            samples[i] = _unif_sample([0,1])
        # require sample mean be within 3 standard deviations of true
        # mean under null hypothesis
        sig_mu =  3.0/np.sqrt(12*N)
        self.assertTrue(np.abs(np.mean(samples) - 0.5) < sig_mu)


if __name__ == '__main__':
    unittest.main()
